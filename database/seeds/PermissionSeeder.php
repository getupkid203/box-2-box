<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        DB::beginTransaction();
        Schema::disableForeignKeyConstraints();
        Permission::truncate();
        Role::truncate();
        Schema::enableForeignKeyConstraints();

        $roles = [
            'super admin',
            'administrator',
            'editor',
            'subscriber'
        ];

        foreach($roles as $r) {
            Role::create([ 'name' => $r ]);
        }

        $user = \b2b\Models\User::find(1);
        $user->assignRole('super admin');

        DB::commit();
    }
}
